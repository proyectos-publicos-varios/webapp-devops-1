package org.example.controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class HelloController {
    @GetMapping("/saludar")
    public String saludar() {
        return "Hola Mundo DevOps";
    }
}